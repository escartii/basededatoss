**[Enlace a mi repositorio](https://gitlab.com/escartii/basededatoss)**

# Consultas Instagram

### Ejercicio 1: Fotos del usuario con ID 36.

**[Enlace el archivo de la consulta](./querys/query1.sql)**

```sql
select * from instagram_low_cost.fotos f1
where f1.idUsuario=36
```

---

### Ejercicio 2: Fotos del usuario con ID 36 tomadas en enero del 2023.


Buscamos en la tabla fotos y filtramos por año 2024 y por mes 01 y que sean del usuario 36, pero como no había lo modifiqué y busqué del usuario 33, el resultado es el mismo.

```sql
select * from instagram_low_cost.fotos f1
where YEAR(f1.fechaCreacion) = 2024  
AND MONTH(f1.fechacreacion) = 01
AND f1.idUsuario=33;
```

**[Enlace el archivo de la consulta](./querys/query2.sql)**

---

### Ejercicio 3: Comentarios del usuario 36 sobre la foto 12 del usuario 11.


cogemos las dos tablas, comentarios y fotos, buscamos en comentarios del idUsuario 36 y que sea sobre la foto 12 del usuario11.

```sql
SELECT usuarios.nombre,comentarios.comentario,fotos.url
	FROM comentarios,fotos,usuarios, comentariosFotos
    WHERE fotos.idUsuario = 11
        AND usuarios.idUsuario = 36
        AND usuarios.idUsuario = comentarios.idUsuario
        AND comentariosFotos.idFoto = fotos.idFoto
        AND fotos.idUsuario = 11
        AND comentarios.idComentario = comentariosFotos.idComentario;
```

**[Enlace el archivo de la consulta](./querys/query3.sql)**

---

### Ejercicio 4: Fotos que han sorprendido al usuario 25.


seleccionamos la tabla fotos, y buscamos en las reacciones el usuario 25 que haya reaccionado con el tipo 4.

```sql
SELECT instagram_low_cost.fotos.*
FROM instagram_low_cost.fotos, instagram_low_cost.reaccionesFotos
WHERE instagram_low_cost.fotos.idFoto = instagram_low_cost.reaccionesFotos.idFoto
    AND instagram_low_cost.reaccionesFotos.idUsuario = 25
    AND instagram_low_cost.reaccionesFotos.idTipoReaccion = 4;
```

**[Enlace el archivo de la consulta](./querys/query4.sql)**

---

5. **Administradores con Más de 2 Me Gusta:** Obtiene los administradores con más de 2 "Me gusta" en sus interacciones.

    ```sql
    SELECT idAdministrador, COUNT(*) as total_likes
    FROM interacciones
    WHERE tipoInteraccion = 'Me gusta'
    GROUP BY idAdministrador
    HAVING total_likes > 2;
    ```

### Ejercicio 6: Fotos que han sorprendido al usuario 25.

**[Enlace el archivo de la consulta](./querys/query5.sql)**


Buscamos todas las reacciones del usuario 25 que ha sorprendido (el tipo de reaccion 3)

```sql
SELECT COUNT(*)
FROM instagram_low_cost.reaccionesFotos
WHERE instagram_low_cost.reaccionesFotos.idFoto = 12
    AND instagram_low_cost.reaccionesFotos.idUsuario = 45
    AND instagram_low_cost.reaccionesFotos.idTipoReaccion = 3;

```

**[Enlace el archivo de la consulta](./querys/query4.sql)**

---


### Ejercicio 7: Número de fotos tomadas en la playa (en base al título).

Contamos todas las fotos que su descripción contenga la palabra playa

```sql
SELECT COUNT(*) FROM fotos
WHERE fotos.descripcion LIKE '%playa%';
```

**[Enlace el archivo de la consulta](./querys/query7.sql)**

